import React from "react";
import PropTypes from "prop-types";
import { ComponentDisplay } from './Display.styles';

class Display extends React.Component {
  static propTypes = {
    value: PropTypes.string,
  }

  render(){
    return (
      <ComponentDisplay>
        <div>{this.props.value}</div>
      </ComponentDisplay>
    )
  }
}

export default Display;